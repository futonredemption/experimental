#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/highgui/highgui.hpp"
#include <iostream>
#include <memory>
#include <string>
#include <chrono>

#include <grpc/grpc.h>
#include <grpc++/server.h>
#include <grpc++/server_builder.h>
#include <grpc++/server_context.h>
#include <grpc++/server_credentials.h>
#include <grpc++/status.h>
#include "image_service.grpc.pb.h"

using grpc::Server;
using grpc::ServerBuilder;
using grpc::ServerContext;
using grpc::Status;
using image_service::ApplyEffectRequest;
using image_service::ApplyEffectResponse;
using image_service::ImageService;
using image_service::ImageFormat;
using std::chrono::system_clock;
using std::chrono::time_point;
using std::chrono::seconds;


/// Global Variables
int DELAY_CAPTION = 1500;
int DELAY_BLUR = 100;
int MAX_KERNEL_LENGTH = 31;

time_point<system_clock> GetNow();

int rpc_count = 0;
time_point<system_clock> start_time = GetNow();

time_point<system_clock> GetNow() {
  return system_clock::now();
}

class ImageServiceImpl final : public ImageService::Service {
  Status ApplyEffect(ServerContext* context,
    const ApplyEffectRequest* request,
    ApplyEffectResponse* response) override {
    rpc_count++;
    if (rpc_count % 100 == 0 && rpc_count > 100) {
      time_point<system_clock> now = GetNow();
      
      std::cout << "RPCs " << rpc_count << "\t QPS: " << rpc_count / std::chrono::duration_cast<seconds>(now - start_time).count() << "\n";
    }
    
    std::string image_str = request->image_data();
    
    //std::cout << "Image Data: " << image_str.size() << std::endl;
  
    std::vector<char> image_data(image_str.begin(), image_str.end());
    cv::Mat raw_data(image_data);
    cv::Mat src = cv::imdecode(raw_data, 1);
    cv::Mat dst = src.clone();
    for ( int i = 1; i < MAX_KERNEL_LENGTH; i = i + 2 ) { 
      GaussianBlur( src, dst, cv::Size( i, i ), 0, 0 );
    }
    
    
    std::vector<int> params;
    const char* extension;
    switch (request->image_format().format()) {
      case ImageFormat::JPEG:
        extension = ".jpg";
        params.push_back(CV_IMWRITE_JPEG_QUALITY);
        params.push_back(100);
        break;
      case ImageFormat::PNG:
        extension = ".png";
        params.push_back(CV_IMWRITE_PNG_COMPRESSION);
        params.push_back(9);
        break;
      case ImageFormat::WEBP:
        extension = ".webp";
        break;
    }
    std::vector<uchar> buffer;
    cv::imencode(extension, dst, buffer, params);
    
    //std::cout << "Result Image Data: " << buffer.size() << std::endl;
    
    std::string result(buffer.begin(), buffer.end());
    response->set_image_data(result);
    return Status::OK;
  }
};

void StartGrpcServer() {
  std::string server_address("0.0.0.0:50051");
  ImageServiceImpl service;

  ServerBuilder builder;
  builder.AddListeningPort(server_address, grpc::InsecureServerCredentials());
  builder.RegisterService(&service);
  std::unique_ptr<Server> server(builder.BuildAndStart());
  std::cout << "Server listening on " << server_address << std::endl;
  server->Wait();
}

int main(int argc, char**  argv) {
  StartGrpcServer();
}

/*
 int main( int argc, char** argv )
 {
   /// Load the source image
   src = imread( "tires.jpeg", 1 );

   dst = src.clone();

    for ( int i = 1; i < MAX_KERNEL_LENGTH; i = i + 2 ) { 
      GaussianBlur( src, dst, Size( i, i ), 0, 0 );
     }

     imwrite("output.jpg", dst);
     return 0;
 }
*/