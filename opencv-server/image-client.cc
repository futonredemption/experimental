#include <iostream>
#include <memory>
#include <string>
#include <fstream>

#include <grpc/grpc.h>
#include <grpc++/channel_arguments.h>
#include <grpc++/channel_interface.h>
#include <grpc++/client_context.h>
#include <grpc++/create_channel.h>
#include <grpc++/credentials.h>
#include <grpc++/status.h>
#include "image_service.grpc.pb.h"

using grpc::ChannelArguments;
using grpc::ChannelInterface;
using grpc::ClientContext;
using grpc::Status;
using image_service::ApplyEffectRequest;
using image_service::ApplyEffectResponse;
using image_service::ImageService;

class ImageServiceClient {
 public:
  ImageServiceClient(std::shared_ptr<ChannelInterface> channel)
      : stub_(ImageService::NewStub(channel)) {}

  std::string ApplyEffect(const std::string& image_data) {
    ApplyEffectRequest request;
    request.set_image_data(image_data);
    ApplyEffectResponse response;
    ClientContext context;

    Status status = stub_->ApplyEffect(&context, request, &response);
    if (status.ok()) {
      return response.image_data();
    } else {
      return "Rpc failed";
    }
  }

 private:
  std::unique_ptr<ImageService::Stub> stub_;
};

int main(int argc, char** argv) {
  ImageServiceClient imageService(
      grpc::CreateChannel("localhost:50051", grpc::InsecureCredentials(),
                          ChannelArguments()));
                          
  std::ifstream file("tires.jpeg", std::ios::binary);
  file.seekg(0, std::ios::end);
  std::streamsize size = file.tellg();
  file.seekg(0, std::ios::beg);
  
  std::vector<char> buffer(size);
 
  if (file.read(buffer.data(), size))
  {
      /* worked! */
  }
  std::string image_data(buffer.begin(), buffer.end());
  std::cout << "Image Data: " << image_data.size() << std::endl;
  std::string reply;
  int rpc_count = 0;
  for (;;) {
    rpc_count++;
    reply = imageService.ApplyEffect(image_data);
    if (rpc_count % 10 == 0) {
      std::printf("RPCs: %d\t", rpc_count);
    }
  }
  
  std::cout << "Result Image Data: " << reply.size() << std::endl;
  
  std::ofstream fout;
  fout.open("output.jpg", std::ios::binary | std::ios::out);
  fout.write(reply.c_str(), sizeof(char) * reply.size());
  fout.close();
  
  return 0;
}
