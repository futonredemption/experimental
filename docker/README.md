Docker Images
=============

Get Images
----------
To start using the images without any fuss.

	sudo docker pull jeremyje/microservices-dev:devel
	sudo docker run -it -d -p 80:80 -v ${HOME}/development/experimental/:/workspace/ jeremyje/microservices-dev:devel

Building
--------
Cloud9+Bazel+Grpc

	./build_googleide.sh

Google IDE+OpenCV

	./build_microservicesdev.sh

A set of docker files that I use to create portable development environments and other things.

You can pull them from Docker Hub
https://hub.docker.com/r/jeremyje/

* These images are not endorsed or supported by Google, Docker or anyone else for that matter.
