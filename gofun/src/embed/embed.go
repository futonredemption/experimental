package embed

import (
	"archive/zip"
	"bytes"
	"encoding/base64"
	"io"
)

func ListEmbeddedResourceFiles(data string) ([]string, error) {
	var files []string
	reader, err := loadEmbeddedResourceAsZip(data)
	if err != nil {
		return nil, err
	}
	for _, f := range reader.File {
		files = append(files, f.Name)
	}
	return files, nil
}

func OpenEmbeddedResourceFile(data string, path string) (io.ReadCloser, error) {
	reader, err := loadEmbeddedResourceAsZip(data)
	if err != nil {
		return nil, err
	}
	for _, f := range reader.File {
		if f.Name == path {
			return f.Open()
		}
	}
	return nil, nil
}

func loadEmbeddedResourceAsZip(data string) (*zip.Reader, error) {
	binary_data, err := base64.StdEncoding.DecodeString(data)
	if err != nil {
		return nil, err
	}
	buf := bytes.NewReader(binary_data)
	return zip.NewReader(buf, int64(len(binary_data)))
}

func check(e error) {
	if e != nil {
		panic(e)
	}
}
