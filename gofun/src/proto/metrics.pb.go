// Code generated by protoc-gen-go.
// source: metrics.proto
// DO NOT EDIT!

/*
Package proto is a generated protocol buffer package.

It is generated from these files:
	metrics.proto

It has these top-level messages:
	CheckHealthRequest
	CheckHealthResponse
*/
package proto

import proto1 "github.com/golang/protobuf/proto"

import (
	context "golang.org/x/net/context"
	grpc "google.golang.org/grpc"
)

// Reference imports to suppress errors if they are not otherwise used.
var _ context.Context
var _ grpc.ClientConn

// Reference imports to suppress errors if they are not otherwise used.
var _ = proto1.Marshal

type CheckHealthRequest struct {
}

func (m *CheckHealthRequest) Reset()         { *m = CheckHealthRequest{} }
func (m *CheckHealthRequest) String() string { return proto1.CompactTextString(m) }
func (*CheckHealthRequest) ProtoMessage()    {}

type CheckHealthResponse struct {
	Ok bool `protobuf:"varint,1,opt,name=ok" json:"ok,omitempty"`
}

func (m *CheckHealthResponse) Reset()         { *m = CheckHealthResponse{} }
func (m *CheckHealthResponse) String() string { return proto1.CompactTextString(m) }
func (*CheckHealthResponse) ProtoMessage()    {}

// Client API for Metrics service

type MetricsClient interface {
	CheckHealth(ctx context.Context, in *CheckHealthRequest, opts ...grpc.CallOption) (*CheckHealthResponse, error)
}

type metricsClient struct {
	cc *grpc.ClientConn
}

func NewMetricsClient(cc *grpc.ClientConn) MetricsClient {
	return &metricsClient{cc}
}

func (c *metricsClient) CheckHealth(ctx context.Context, in *CheckHealthRequest, opts ...grpc.CallOption) (*CheckHealthResponse, error) {
	out := new(CheckHealthResponse)
	err := grpc.Invoke(ctx, "/proto.Metrics/CheckHealth", in, out, c.cc, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

// Server API for Metrics service

type MetricsServer interface {
	CheckHealth(context.Context, *CheckHealthRequest) (*CheckHealthResponse, error)
}

func RegisterMetricsServer(s *grpc.Server, srv MetricsServer) {
	s.RegisterService(&_Metrics_serviceDesc, srv)
}

func _Metrics_CheckHealth_Handler(srv interface{}, ctx context.Context, codec grpc.Codec, buf []byte) (interface{}, error) {
	in := new(CheckHealthRequest)
	if err := codec.Unmarshal(buf, in); err != nil {
		return nil, err
	}
	out, err := srv.(MetricsServer).CheckHealth(ctx, in)
	if err != nil {
		return nil, err
	}
	return out, nil
}

var _Metrics_serviceDesc = grpc.ServiceDesc{
	ServiceName: "proto.Metrics",
	HandlerType: (*MetricsServer)(nil),
	Methods: []grpc.MethodDesc{
		{
			MethodName: "CheckHealth",
			Handler:    _Metrics_CheckHealth_Handler,
		},
	},
	Streams: []grpc.StreamDesc{},
}
