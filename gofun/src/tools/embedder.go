package main

import (
	"archive/zip"
	"bytes"
	"encoding/base64"
	"flag"
	"fmt"
	"io/ioutil"
	"os"
	"path/filepath"
	"strings"
	"embed"
)

var (
	directory    = flag.String("directory", "", "Data Directory to convert into Go source.")
	archive_file = flag.String("file", "", "Data file to convert into Go source.")
	outfile      = flag.String("output", "outfile", "Output")
	package_name = flag.String("package", "datafile", "Data File")
)

func main() {
	flag.Parse()
	var data []byte
	if archive_file != nil && *archive_file != "" {
		data = GetArchiveFile(*archive_file)
	} else if directory != nil && *directory != "" {
		data = GetDirectoryAsArchive(*directory)
	}

	encoded := base64.StdEncoding.EncodeToString(data)
	file_contents := ToGoSourceFile(*package_name, encoded)
	WriteEmbeddedSourceFile(*outfile + ".go", file_contents)
	again_reader, err := embed.OpenEmbeddedResourceFile(encoded, "testdata/serverdata/server1.key")
	check(err)
	again_data, err := ioutil.ReadAll(again_reader)
	check(err)
	again_string := string(again_data[:])
	fmt.Printf(again_string)
}

func GetArchiveFile(archive_file string) []byte {
	data, err := ioutil.ReadFile(archive_file)
	check(err)
	return data
}

type fileData struct {
	Name    string
	Content []byte
}

func GetDirectoryAsArchive(directory string) []byte {
	var files []fileData

	walkFunc := func(path string, info os.FileInfo, err error) error {

		stat, err := os.Stat(path)
		if err != nil {
			return err
		}

		if err != nil {
			return err
		}
		fmt.Println(path)
		if !stat.IsDir() {
			content, err := ioutil.ReadFile(path)
			check(err)
			path = strings.Replace(path, "\\", "/", -1)
			files = append(files, fileData{Name: path, Content: content})
		}
		return nil
	}
	buf := new(bytes.Buffer)
	w := zip.NewWriter(buf)
	err := filepath.Walk(directory, walkFunc)

	for _, file := range files {
		f, err := w.Create(file.Name)
		check(err)
		_, err = f.Write([]byte(file.Content))
		check(err)
	}

	err = w.Close()
	check(err)
	return buf.Bytes()
}

func ToGoSourceFile(package_name string, encoded string) string {
	template := `package %s
	const Data string = ` + "`%s`" + `
	`
	file_contents := fmt.Sprintf(template, package_name, encoded)
	return file_contents
}

func WriteEmbeddedSourceFile(outfile_path string, file_contents string) {
	err := ioutil.WriteFile(outfile_path, []byte(file_contents), 0644)
	check(err)
}

func check(e error) {
	if e != nil {
		panic(e)
	}
}
