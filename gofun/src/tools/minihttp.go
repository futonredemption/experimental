package main

import (
	"fmt"
	"net/http"
	"embed"
	"io/ioutil"
	"strings"
)

func rootHandler(w http.ResponseWriter, r *http.Request) {
	path := strings.TrimLeft(r.URL.Path, "/")
	fmt.Printf("GET %s\n", path)
	
	again_reader, err := embed.OpenEmbeddedResourceFile(embed.Data, path)
	check(err)
	if again_reader == nil {
		fmt.Printf("NOT FOUND\n")
		fmt.Fprintf(w, "Not Found: %s\n", path)
		return
	}
	again_data, err := ioutil.ReadAll(again_reader)
	check(err)
	again_string := string(again_data[:])
	fmt.Fprintf(w, again_string)
}

func main() {
	http.HandleFunc("/", rootHandler)
	http.ListenAndServe(":8080", nil)
}

func check(e error) {
	if e != nil {
		panic(e)
	}
}
