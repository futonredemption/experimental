#!/bin/bash

sudo apt-get update -y
sudo apt-get install -y golang git make

git clone https://futonredemption@bitbucket.org/futonredemption/experimental.git
cd experimental/gofun
export GOPATH=$PWD
export PATH=$PATH:$PWD/bin
go get -u github.com/golang/protobuf/{proto,protoc-gen-go}
go get -u google.golang.org/grpc
make
