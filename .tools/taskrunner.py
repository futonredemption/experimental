import logging
import os
import subprocess
import sys

def BuildProtoGo(context):
	proto_file = context['file_path']
	base_dir = os.path.dirname(proto_file)
	Run('protoc --proto_path="%s" %s --go_out=plugins=grpc:%s' % (base_dir, proto_file, base_dir))

def RunGo(context):
	go_file = context['file_path']
	Run('go run %s' % (go_file))


def main():
	context = {
		'cmd': sys.argv[1],
		'file_path': sys.argv[2],
		'workspace_root': sys.argv[3],
		'cwd': sys.argv[4],
	}
	if context['cmd'] == 'build-proto-go':
		BuildProtoGo(context)
	elif context['cmd'] == 'run-go':
		RunGo(context)
	else:
		logging.warning("Bad Command: %s", context)

def Run(args):
	print args
	subprocess.call(args, shell=True)

main()
